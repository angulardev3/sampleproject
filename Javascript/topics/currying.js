// function addition(a,b,c){
//     return a+b+c;
// }
// let res=addition(2,3,4);
// console.log(res);
// example-1 
// function addition(a){
//     return function(b){
//         return function(c){
//             return a+b+c
//         }
//     }
// }
// let res = addition(2);
// let data=res(3);
// let data2=data(4)
// console.log(data2);// this is wrong way to print this is correct but complex. use currying to solve
// let res=addition(2)(3)(4);
// console.log(res);

// example-2
// userObj={
//     name:'Manoj',
//     age:32
// }

// function userInfo(obj){
//     return function(userInfo){
//         return obj[userInfo]
//     }
// }
// let res=userInfo(userObj);
// console.log(res('name'));

// example-3
//Infinite currying
function sum(a){
    return function(b){
        if(b){
            return sum(a+b);
        } 
        return a;
    }
}
// console.log(sum(2)(3)(5)(6)(7));//check output
  console.log(sum(1)(2)(3)(4)(5)(6)());  //21
// function sum(a) {
//     return function(b){
//       if(b)
//         return sum(a+b);
      
//       return a;
//     }
//   }
//   console.log(sum(1)(2)(3)(4)(5)(6)());  //21