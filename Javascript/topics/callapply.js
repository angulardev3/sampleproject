// //Call, Apply & Bind

// let userDetails={
//     name:'Manoj',
//     age:28,
//     designation:'UI Developer',
//     printDetails:function(){
//         console.log(this.age)
//     }
// }
// userDetails.printDetails();

// let userDetails2={
//     name:"Beera",
//     age:30,
//     Designation:'Higher'
// }
// userDetails.printDetails.call(userDetails2);

//Apply method
let userDetails={
    name:"Manoj",
    age:25
}
let printFullname=function(hometown,state){
    console.log(this.name+" " + this.age + " " + hometown + " " + state + " ")
}
// printFullname.apply(userDetails,["chikodi","umarani"]);


let printMyname=printFullname.bind(userDetails,"chikodi","umarani");
console.log(printMyname);
printMyname();

// const obj={name:'john'};
// let greeting=function(a,b){
//     return `${a} ${this.name} ${b}`
// }
// console.log(greeting.call(obj,"Hello", "Are you learning"));
// console.log(greeting.apply(obj,["Hi","Are you in method"]));

// let bound=greeting.bind(obj);
// console.log(bound("Manoj","How much you learned"))


