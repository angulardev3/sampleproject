import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NgModule } from '@angular/core';

@NgModule ({
  imports: [
    MatSlideToggleModule,
  ]
}) 
export interface MaterialModule {}