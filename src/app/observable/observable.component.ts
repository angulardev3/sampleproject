import { Component, OnInit } from '@angular/core';
import { of,Observable, filter, interval, pipe } from 'rxjs';
import { fromEvent, scan, debounce } from 'rxjs';

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.css']
})
export class ObservableComponent implements OnInit {
  orderStatus :any;
  // dataValue: Observable<any>;
  

  constructor(){}
  ngOnInit():void{

const clicks = fromEvent(document, 'click');
const result = clicks.pipe(
  scan(i => ++i, 1),
  debounce(i => interval(200 * i))
);
result.subscribe(x => console.log(x));
    // let test=of(1,2,3,4,5);
    // test.subscribe({
    //   next:value=>console.log('next :', value)
    // })
    // let case1=test.pipe(
    //   filter(x=>x%5===0),
      
    // )
    // case1.subscribe((x)=>{
    //   console.log(x);
    // })
    // new Observable(observer=>{
    //   setTimeout(()=>{
    //     observer.next('Processing');
    //   },2000)
    //   setTimeout(()=>{
    //     observer.next('completed');
    //   },3000)
    //   setTimeout(()=>{
    //     observer.complete();
    //   },4000)
    //   setTimeout(()=>{
    //     observer.next("again processing");
    //   },4000)
    //   setTimeout(()=>{
    //     observer.error();
    //   },5000)
    // }).subscribe(val=>{
    //   this.orderStatus=val;
    // })
    // const Observable=interval(10);
    // Observable.subscribe((number)=>{
    //   console.log(number);
    // })
    // Observable.pipe(
    //   filter((number)=>{
    //     return number%2===0;
    //   })
    // ).subscribe((number)=>{
    //   console.log(number);
    // })
  }

}
