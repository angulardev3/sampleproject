import { Component, OnInit } from '@angular/core';
import { Observable,Subject,filter,from,fromEvent, map, tap } from 'rxjs';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styleUrls: ['./rxjs.component.css']
})
export class RxjsComponent implements OnInit {
// const obs=new Observable(subscriber=>subscriber.next('Hello'));
// obs.subscribe((data)=>console.log(data))
ngOnInit(){
  // const sourse=from([1,2,3,4]);
  // sourse.subscribe(data=>{
  //   console.log("from data",data);
  // })
// const sourse=fromEvent(document,'click');
// sourse.subscribe(data=>{
//   console.log("clicked",data)
// })
// sourse.subscribe(data=>{
//   console.log("clicked",data)
// })
// const source=new Subject<string>();
// source.subscribe(data=>{
//   console.log(data);
// })
// let count=0;
// fromEvent(document,'click').subscribe(()=>{
// source.next('hello'+ ++count);
// })
fromEvent<MouseEvent>(document,'click')
.pipe(
  tap(data=>console.log('Before filtering',data)),
  filter(data=>data.clientX > 300 && data.clientY>300),
  map(data=>`X: ${data.clientX},Y:${data.clientY}`)
  )

.subscribe((data)=>{
  console.log(data);
})
}


}