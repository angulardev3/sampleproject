import { Directive,ElementRef } from '@angular/core';

@Directive({
  selector: '[appAttribute]'
})
export class AttributeDirective {

  constructor(private elementRef:ElementRef) { 
    elementRef.nativeElement.style.color="red"
  }

}
