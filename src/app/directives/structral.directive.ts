import { Directive,Input,TemplateRef,ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appStructral]'
})
export class StructralDirective {

  constructor(
    private templateRef:TemplateRef<any>,
    private containerRef:ViewContainerRef)
    {}
   
    @Input() set appStructral(condition: boolean) {
      console.log("boolean value",condition)
      if(!condition){
        this.containerRef.createEmbeddedView(this.templateRef)
      }else{
        this.containerRef.clear();
      }
    }
}
