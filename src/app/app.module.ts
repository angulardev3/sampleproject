import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {CommonModule} from '@angular/common'
import { AppComponent } from './app.component';
import { PracticeComponent } from './practice/practice.component';
import {MessageService} from './services/message.service';
import { PropertybindigComponent } from './propertybindig/propertybindig.component'
import { FormsModule } from '@angular/forms';
import { Project1Component } from './components/project1/project1.component';
import { ParentComponent } from './components/project2/parent/parent.component';
import { ChildComponent } from './components/project2/child/child.component';
import {RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { UsersComponent } from './components/users/users.component';
import { ErrorComponent } from './error/error.component';
import {ApiService} from './services/api.service';
import  {HttpClientModule} from '@angular/common/http';
import { AttributeDirective } from './directives/attribute.directive';
import { StructralDirective } from './directives/structral.directive';
import { FilterPipe } from './filter.pipe';
import { RxjsComponent } from './rxjs/rxjs.component';
import { MaterialComponent } from './material/material/material.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ObservableComponent } from './observable/observable.component';




const appRoute:Routes=[
  // {path:'',component:HomeComponent},
  {path:'', redirectTo:'home', pathMatch:'full'},
  {path:'home', component:HomeComponent},
  {path:'users', component:UsersComponent},
  {path:'about', component:AboutComponent},
  {path:'**', component:ErrorComponent}

]
@NgModule({
  declarations: [
    AppComponent,
    PracticeComponent,
    PropertybindigComponent,
    Project1Component,
    ParentComponent,
    ChildComponent,
    HomeComponent,
    AboutComponent,
    UsersComponent,
    ErrorComponent,
    AttributeDirective,
    StructralDirective,
    FilterPipe,
    RxjsComponent,
    MaterialComponent,
    ObservableComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule,
    RouterModule.forRoot(appRoute),
    HttpClientModule,
    MatSlideToggleModule,
    BrowserAnimationsModule,
  ],
  providers: [
    MessageService,
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
