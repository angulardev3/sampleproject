import { Component } from '@angular/core';

@Component({
  selector: 'app-practice',
  templateUrl: './practice.component.html',
  styleUrls: ['./practice.component.css']
})
export class PracticeComponent {
  dateToday: string="";
  name: string="";
  currency: number = 50;
  percentNumber:number=.20;
  decimalNumber: number = 12.638467846;
  constructor() { }

  ngOnInit(): void {
 
    this.dateToday = new Date().toDateString();
    
    this.name = "Simplilearn"

  } 
}
