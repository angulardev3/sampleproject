import { Injectable } from '@angular/core';
import {Observable} from "rxjs"
import {HttpClient} from "@angular/common/http";
import { signUp } from '../data-type';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  readonly Root_URL;

  constructor( private httpClient:HttpClient) {
    this.Root_URL="http://localhost:3000";
   }

   userSignUp(data:signUp){
    return this.httpClient.post("http://localhost:3000/seller",data)
   }

   getUserDetails():Observable<any>{
   const baseUrl='https://jsonplaceholder.org/posts/data'
   return this.httpClient.get<any>(baseUrl)
   }

}

