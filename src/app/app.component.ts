import { Component, OnInit } from '@angular/core';
import {MessageService} from './services/message.service'
import {ApiService} from './services/api.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'myproject';
  msg:string="";
  userid=1
  constructor(private _messageService:MessageService, private _apiservice:ApiService){

  }

  ngOnInit(){
// console.log(this._apiservice.getUserDetails)
this._apiservice.getUserDetails().subscribe(
  (res)=>{
      console.log(res)
   
  }
)
  }

  getmessage(){
    this.msg=this._messageService.getService();

  }
}
