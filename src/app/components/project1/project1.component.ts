import { Component,OnInit,OnChanges } from '@angular/core';

@Component({
  selector: 'app-project1',
  templateUrl: './project1.component.html',
  styleUrls: ['./project1.component.css']
})
export class Project1Component  {
 
  // Structural directives
  isCustomer=false;
  customers=[
    "global","domestic","National"
  ]
  concepts='componentdirective'
  styleprop="red"
  constructor(){
  }

}
