import { Component,OnInit,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  // @Input() parentData:any;
  @Output() childData:EventEmitter<any>=new EventEmitter()
  constructor(){}

  ngOnInit(){
    
}
sendData(){
  let childObj={
    name:'I am Manojkumar',
    domain:'Angular devloper'
  }
  this.childData.emit(childObj)

}

}
