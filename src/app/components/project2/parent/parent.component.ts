import { Component,ViewChild,AfterViewInit,ElementRef, ViewChildren, QueryList } from '@angular/core';
import { ChildComponent } from '../child/child.component';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements AfterViewInit {
  name:string="";
  // data="parentHere";
  // dataObj={
  //   name:'manoj',
  //   place:'bangalore'
  // }
  // @ViewChild('someInput') someInput!: ElementRef;
  @ViewChildren ('someInput') someInput!:QueryList<ElementRef>;

  // @ViewChild(ChildComponent) print!:ChildComponent;
  constructor(){

  }
  ngAfterViewInit(): void {
    // this.someInput.nativeElement.value = 'Whale!';
   
    this.someInput.forEach((el)=>{
      console.log(el.nativeElement.value)
    });
  }
  // parentFunction(data:any){
  //  this.name=data.name;
  // }

}
