const express = require('express');
const app = express();

var cors=require('cors');
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// const user=require('.user/userroutes');
// app.use('/api/user',user);

app.listen(3000, () => {
  console.log('Listening on port 3000.');
});